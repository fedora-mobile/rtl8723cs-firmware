Name:    rtl8723cs-firmware
Summary: Proprietary realtek bluetooth firmware
Version: 1.0
Release: 4%{?dist}

License: Redistributable, no modification permitted
URL:     https://github.com/anarsoul/rtl8723bt-firmware
Source0: rtl8723cs_xx_config-pinephone.bin
Source1: rtl8723cs_xx_fw.bin
Source2: anx7688-fw.bin
Source3: ov5640_af.bin
Source4: rtl8723cs_xx_config.bin

%description
Firmware for the pinephone.

%prep
echo No prep neccesary, proprietary.

%build
echo No build neccesary, proprietary.

%install
mkdir -p ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE0} ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE4} ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE1} ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE2} ${RPM_BUILD_ROOT}/lib/firmware/
cp %{SOURCE3} ${RPM_BUILD_ROOT}/lib/firmware/

%check
echo No way to check, proprietary.

%files
/lib/firmware/anx7688-fw.bin
/lib/firmware/ov5640_af.bin
/lib/firmware/rtl_bt/*

%changelog
* Thu Feb 10 2022 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.0-4
- Adding compat bin

* Thu Nov 05 2020 Nikhil Jha <hi@nikhiljha.com> - 1.0-3
- update anx7688 driver
- add ov5640_af firmware

* Tue Jun 16 2020 Nikhil Jha <hi@nikhiljha.com> - 1.0-2
- added anx7688 driver

* Tue Mar 03 2020 Nikhil Jha <hi@nikhiljha.com> - 1.0-1
- Initial packaging
